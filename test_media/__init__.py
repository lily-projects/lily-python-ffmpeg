"""
Test media package
"""

import os


_TEST_MEDIA_ITEMS = [os.path.join(os.path.dirname(__file__), 'trailer_minions_2015.mp4')]


def get_list_of_test_media():
    return _TEST_MEDIA_ITEMS


def get_default_test_media():
    return _TEST_MEDIA_ITEMS[0]


def get_test_media():
    while True:
        print('Select a test media:')
        for i, media in enumerate(get_list_of_test_media()):
            print('%s: %s' % (i + 1, media))
        print('%s: Enter custom URI' % (i + 2))

        input_text = input('\nSelect media [1-%d, q=quit]: ' % (len(_TEST_MEDIA_ITEMS) + 1))
        if input_text == 'q':
            input_text = None
            break

        try:
            index = int(input_text) - 1
            assert index >= 0
            if index == len(_TEST_MEDIA_ITEMS):
                input_text = input('Enter custom URI: ')
                if input_text == '':
                    raise
            else:
                input_text = _TEST_MEDIA_ITEMS[index]
            break
        except (Exception, ):
            print('Invalid input\n')

    return input_text


if __name__ == "__main__":

    testMedia = get_test_media()
    print('\nYou have chosen:', testMedia)

    print('\nDone')
