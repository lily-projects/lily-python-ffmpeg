"""
WX widget for displaying the decoded output of FFmpeg
"""

import threading
import wx

from ffmpeg import FFmpeg


class ViewFFmpeg(wx.Panel):

    _BACKGROUND_COLOUR = '#333333'
    _STATUS_UPDATE_INTERVAL = 500

    def __init__(self, parent):
        super(ViewFFmpeg, self).__init__(parent)
        self.SetBackgroundColour(self._BACKGROUND_COLOUR)

        self._video_image = wx.Panel(self, -1)
        self._video_image.SetDoubleBuffered(True)
        self._video_image.Hide()

        self._ffmpeg = FFmpeg()
        self._media_uri = ''
        self._bitmap = None
        self._decoder_start_error = ''

        self.Bind(wx.EVT_SIZE, self._on_resize)
        self._video_image.Bind(wx.EVT_PAINT, self._on_video_image_paint)

        self._status_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self._on_status_timer, self._status_timer)
        self._status_timer.Start(self._STATUS_UPDATE_INTERVAL)
        self._status_timer.Notify()

    def __del__(self):
        self.stop()

    ###########
    # Private #
    ###########

    def _size_video(self):
        video_left = 0
        video_top = 0

        panel_width, panel_height = self.GetClientSize().Get()
        video_width, video_height = self._video_image.GetSize().Get()
        panel_aspect_ratio = panel_width / panel_height
        video_aspect_ratio = video_width / video_height
        if panel_aspect_ratio < video_aspect_ratio:
            video_width = panel_width
            video_height = int(video_width / video_aspect_ratio)
            video_top = int((panel_height - video_height) / 2)

        else:
            video_height = panel_height
            video_width = int(video_height * video_aspect_ratio)
            video_left = int((panel_width - video_width) / 2)

        self._video_image.SetSize((video_width, video_height))
        self._video_image.SetPosition((video_left, video_top))

    def _output_video(self, image):
        image.Rescale(self._video_image.GetSize().Get()[0], self._video_image.GetSize().Get()[1])
        self._bitmap = wx.Bitmap(image)
        self._video_image.Refresh()

    def _start_decoder(self):
        self._decoder_start_error = ''
        info = {}

        try:
            info = self._ffmpeg.get_media_info(self._media_uri)
            if list(info.keys()) == 0:
                raise Exception('JSON object is empty')
        except Exception as e:
            self._decoder_start_error = 'No media information received.\n%s' % e

        try:
            video_info = list(filter(lambda x: x['codec_type'] == 'video', info['streams']))[0]
            self._width = video_info['width']
            self._height = video_info['height']
        except (Exception, ):
            self._decoder_start_error = 'Invalid media information received.'

        if self._decoder_start_error == '':
            self._bitmap = wx.Bitmap(self._width, self._height)
            self._video_image.SetSize((self._width, self._height))
            self._video_image.Refresh()
            self._video_image.Show()
            self._size_video()
            self._ffmpeg.start_decode_media_to_buffer(self._media_uri, self)

    ##################
    # Event handlers #
    ##################

    def _on_resize(self, event):
        self._size_video()
        event.Skip()

    def _on_video_image_paint(self, event):
        if self._bitmap is not None:
            dc = wx.BufferedPaintDC(self._video_image)
            dc.Clear()
            dc.DrawBitmap(self._bitmap, 0, 0)
        event.Skip()

    def _on_status_timer(self, event):
        if not self.is_decoding():
            self._video_image.Hide()
        event.Skip()

    ##########
    # Public #
    ##########

    def write(self, data):
        try:
            wx.CallAfter(self._output_video, wx.Image(self._width, self._height, data))
        except (Exception, ):
            pass

    def set_media_uri(self, media_uri):
        self._media_uri = media_uri

    def start(self):
        t = threading.Thread(target=self._start_decoder)
        t.daemon = True
        t.start()
        while t.is_alive():
            wx.Yield()

        # Check for decoder start error
        if self._decoder_start_error != '':
            raise Exception(self._decoder_start_error)

    def is_decoding(self):
        return self._ffmpeg.is_decoder_running()

    def stop(self):
        if self._ffmpeg.is_decoder_running():
            self._ffmpeg.stop_decode_media_to_buffer()
            self._video_image.Hide()


if __name__ == '__main__':

    from test_media import get_list_of_test_media


    class TestFrame(wx.Frame):

        _WINDOW_TITLE = 'View FFmpeg Test'
        _WINDOW_SIZE = (1000, 600)

        def __init__(self):
            super(TestFrame, self).__init__(None, -1, self._WINDOW_TITLE, size=self._WINDOW_SIZE)

            panel = wx.Panel(self)
            self._view_ffmpeg = ViewFFmpeg(panel)

            lbl_media = wx.StaticText(panel, -1, 'Select Media:')
            self._cmb_media = wx.ComboBox(panel, -1, choices=get_list_of_test_media())
            self._cmb_media.SetSelection(0)
            self._btn_start_stop = wx.Button(panel, -1, 'Start / Stop')
            self._lbl_decoder_status = wx.StaticText(panel, -1)

            grid = wx.GridBagSizer(10, 10)
            grid.Add(lbl_media, (0, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
            grid.Add(self._cmb_media, (0, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
            grid.Add(self._btn_start_stop, (0, 2), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
            grid.AddGrowableCol(1)

            box = wx.BoxSizer(wx.VERTICAL)
            box.Add(grid, 0, wx.EXPAND | wx.ALL, 10)
            box.Add(self._view_ffmpeg, 1, wx.EXPAND | wx.RIGHT | wx.BOTTOM | wx.LEFT, 5)
            box.Add(self._lbl_decoder_status, 0, wx.EXPAND | wx.ALL, 5)

            panel.SetSizer(box)

            self.Bind(wx.EVT_BUTTON, self._on_start_stop_button, self._btn_start_stop)
            self.SetInitialSize(self._WINDOW_SIZE)

            self._status_timer = wx.Timer(self)
            self.Bind(wx.EVT_TIMER, self._on_status_timer, self._status_timer)
            self._status_timer.Start(1000)
            self._status_timer.Notify()

        def _on_status_timer(self, event):
            if self._view_ffmpeg.is_decoding():
                self._lbl_decoder_status.SetLabel('Decoder is running')
                self._btn_start_stop.SetLabel('Stop')
            else:
                self._lbl_decoder_status.SetLabel('Decoder is idle')
                self._btn_start_stop.SetLabel('Start')
            event.Skip()

        def _on_start_stop_button(self, event):
            if self._view_ffmpeg.is_decoding():
                self._view_ffmpeg.stop()
            else:
                media_uri = self._cmb_media.GetValue()
                if media_uri != '':
                    self._view_ffmpeg.set_media_uri(media_uri)
                    try:
                        self._view_ffmpeg.start()
                    except Exception as e:
                        dlg = wx.MessageDialog(self, str(e), 'Start Decoder', wx.ICON_EXCLAMATION)
                        dlg.ShowModal()
                        dlg.Destroy()
            event.Skip()


    app = wx.App(redirect=False)

    frame = TestFrame()
    frame.Show()

    app.MainLoop()

    print("\nDone")
