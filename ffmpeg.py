"""
Wrapper for ffmpeg.
"""

import json
import os
import re
import subprocess
import sys
import threading
import time

from datetime import datetime

from PIL import Image

from binaries import get_ffmpeg_bin_path
from binaries import get_ffprobe_bin_path


class FFmpeg(object):
    _CREATE_NO_WINDOW = 0x08000000
    _PROCESS_TIME_OUT = 10

    _CMD_VERSION = 'version'
    _CMD_GET_MEDIA_INFORMATION = 'get_media_information'
    _CMD_GET_IMAGE = 'get_image'
    _CMD_DECODE_TO_BUFFER = 'decodeToBuffer'
    _CMD_RECORD_TO_FILE = 'record_to_file'

    def __init__(self):
        self._ffmpeg_bin_path = get_ffmpeg_bin_path()
        self._ffprobe_bin_path = get_ffprobe_bin_path()
        self._decode_thread = None
        self._record_thread = None
        self._recorder_pipe = None
        self._get_output_stop_event = threading.Event()
        self._decode_stop_event = threading.Event()

    ###########
    # Private #
    ###########

    def _generate_command(self, command_type, media_uri="", max_file_duration="", output_file_format=""):
        cmd = []

        verbose = ['-v', 'error']
        input_options = []
        raw_video_output = ['-f', 'image2pipe']
        raw_video_output.extend(['-pix_fmt', 'rgb24'])
        raw_video_output.extend(['-vcodec', 'rawvideo'])
        raw_video_output.append('-')

        if media_uri.startswith('rtsp://'):
            input_options.extend(['-rtsp_transport', 'tcp'])
        else:
            input_options.append('-re')

        if command_type == self._CMD_VERSION:
            cmd.append(self._ffmpeg_bin_path)
            cmd.append('-version')

        elif command_type == self._CMD_GET_MEDIA_INFORMATION:
            cmd.append(self._ffprobe_bin_path)
            cmd.extend(verbose)
            cmd.extend(['-print_format', 'json'])
            cmd.append('-show_streams')
            cmd.append(media_uri)

        elif command_type == self._CMD_GET_IMAGE:
            cmd.append(self._ffmpeg_bin_path)
            cmd.extend(verbose)
            cmd.extend(input_options)
            cmd.extend(['-i', media_uri])
            cmd.append('-an')
            cmd.extend(raw_video_output)

        elif command_type == self._CMD_DECODE_TO_BUFFER:
            cmd.append(self._ffmpeg_bin_path)
            cmd.extend(verbose)
            cmd.extend(input_options)
            cmd.extend(['-i', media_uri])
            cmd.append('-an')
            cmd.extend(raw_video_output)

        elif command_type == self._CMD_RECORD_TO_FILE:
            cmd.append(self._ffmpeg_bin_path)
            cmd.extend(input_options)
            cmd.extend(['-i', media_uri])
            cmd.append('-an')
            cmd.extend(['-c', 'copy'])
            cmd.extend(['-flags', '+global_header'])
            cmd.extend(['-f', 'segment'])
            cmd.extend(['-segment_time', str(max_file_duration)])
            cmd.extend(['-reset_timestamps', '1'])
            cmd.extend(['-strftime', '1'])
            cmd.append(output_file_format)

        return cmd

    def _run_command(self, command, buffer_size=0, log_filename=None):
        creation_flags = 0
        if sys.platform == 'win32':
            creation_flags = self._CREATE_NO_WINDOW

        stdout = subprocess.PIPE
        stderr = None
        if log_filename is not None:
            fp = open(log_filename, "w")
            stdout = fp
            stderr = fp

        return subprocess.Popen(command, bufsize=buffer_size, stdout=stdout, stderr=stderr,
                                creationflags=creation_flags)

    def _terminate_pipe_after_timeout(self, pipe, timeout):
        self._get_output_stop_event.wait(timeout)
        if not self._get_output_stop_event.is_set():
            pipe.terminate()

    def _get_output_from_pipe(self, pipe, read_size=-1, timeout=_PROCESS_TIME_OUT):
        terminate_thread = threading.Thread(target=self._terminate_pipe_after_timeout, args=(pipe, timeout))
        terminate_thread.daemon = True
        self._get_output_stop_event.clear()
        terminate_thread.start()

        output = pipe.stdout.read(read_size)

        if terminate_thread.is_alive():
            self._get_output_stop_event.set()
            terminate_thread.join()

        return output

    def _decode_media_to_buffer(self, media_uri, output_buffer):
        info = self.get_media_info(media_uri)
        video_info = list(filter(lambda x: x['codec_type'] == 'video', info['streams']))[0]

        width = video_info['width']
        height = video_info['height']
        frame_size = width * height * 3

        pipe = self._run_command(self._generate_command(self._CMD_DECODE_TO_BUFFER, media_uri), frame_size * 2)

        n_invalid_frames = 0
        while not self._decode_stop_event.is_set():
            frame = self._get_output_from_pipe(pipe, frame_size)
            if len(frame) == frame_size:
                n_invalid_frames = 0
                output_buffer.write(frame)
            else:
                n_invalid_frames += 1
                if n_invalid_frames == 10:
                    break

        try:
            pipe.terminate()
        except (Exception, ):
            pass

        self._decodeThread = None

    def _record_media_to_video_file(self, media_uri, output_video_name, max_file_duration):
        log_filename = '%s_%s_recorder.log' % (output_video_name, datetime.now().strftime('%Y%m%d_%H%M%S'))
        output_file_format = '%s_%%Y%%m%%d_%%H%%M%%S.flv' % output_video_name
        self._recorder_pipe = self._run_command(
            self._generate_command(self._CMD_RECORD_TO_FILE, media_uri, max_file_duration, output_file_format),
            log_filename=log_filename
        )
        self._recorder_pipe.wait()

    ##########
    # Public #
    ##########

    def get_ffmpeg_version(self):
        pipe = self._run_command(self._generate_command(self._CMD_VERSION))
        output = pipe.stdout.read().decode('latin')
        matches = re.findall(r'ffmpeg version (\d+\.\d+[.\d+]?)', output)
        if len(matches) > 0:
            return matches[0]

        return None

    def get_media_info(self, media_uri):
        pipe = self._run_command(self._generate_command(self._CMD_GET_MEDIA_INFORMATION, media_uri))
        output = self._get_output_from_pipe(pipe)
        return json.loads(output)

    def get_image_from_media(self, media_uri):
        info = self.get_media_info(media_uri)
        video_info = list(filter(lambda x: x['codec_type'] == 'video', info['streams']))[0]
        width = video_info['width']
        height = video_info['height']
        frame_size = width * height * 3

        pipe = self._run_command(self._generate_command(self._CMD_GET_IMAGE, media_uri), frame_size * 2)
        frame = self._get_output_from_pipe(pipe, frame_size)
        pipe.terminate()

        return Image.frombytes('RGB', (width, height), frame)

    def start_decode_media_to_buffer(self, media_uri, output_buffer):
        if self._decode_thread is None:
            self._decode_stop_event.clear()
            self._decode_thread = threading.Thread(target=self._decode_media_to_buffer, args=(media_uri, output_buffer))
            self._decode_thread.daemon = True
            self._decode_thread.start()

    def is_decoder_running(self):
        return self._decode_thread is not None and self._decode_thread.is_alive()

    def stop_decode_media_to_buffer(self):
        if self._decode_thread is not None:
            self._decode_stop_event.set()
            self._decode_thread.join()
            self._decode_thread = None

    def start_recording_media_to_file(self, media_uri, output_video_name, max_file_duration=3600):
        if self._record_thread is None:
            self._record_thread = threading.Thread(target=self._record_media_to_video_file,
                                                   args=(media_uri, output_video_name, max_file_duration))
            self._record_thread.daemon = True
            self._record_thread.start()

    def is_recorder_running(self):
        return self._record_thread is not None and self._record_thread.is_alive()

    def stop_recording_media_to_file(self):
        if self._record_thread is not None:
            if self._recorder_pipe is not None:
                self._recorder_pipe.terminate()
            self._record_thread.join()
            self._record_thread = None


if __name__ == '__main__':

    import tempfile

    from test_media import get_test_media


    class TestBuffer(object):
        @staticmethod
        def write(data):
            print(time.time(), len(data), 'bytes written')

    def get_temp_videos(output_video_name):
        head, tail = os.path.split(output_video_name)
        temp_videos = [x for x in os.listdir(tempfile.gettempdir()) if x.startswith(tail)]
        temp_videos = [os.path.join(head, x) for x in temp_videos]
        return temp_videos


    test_media_uri = get_test_media()
    if test_media_uri is not None:
        ffmpeg = FFmpeg()

        print('\nTest media: %s' % test_media_uri)
        print('ffmpeg version :', ffmpeg.get_ffmpeg_version())

        print('\nGet media information:')
        mediaInfo = ffmpeg.get_media_info(test_media_uri)

        if len(list(mediaInfo.keys())) > 0:
            print('Media info:')
            print(json.dumps(mediaInfo, indent=4))

            print('\nTake image from media:')
            output_image_file = tempfile.mktemp(suffix='.bmp')
            image = ffmpeg.get_image_from_media(test_media_uri)
            image.save(output_image_file)
            print('Image written to:', output_image_file)

            user_input = input('\nTest writing output to buffer (y/n)? ')
            if user_input == 'y':
                print('\nWrite frame to output buffer:')
                ffmpeg.start_decode_media_to_buffer(test_media_uri, TestBuffer())

                i = 5
                while i > 0:
                    time.sleep(1)
                    i -= 1
                    if not ffmpeg.is_decoder_running():
                        break

                if i > 0:
                    print('ERROR: ffmpeg stopped because of a timeout (t = %s)' % i)

                print('\nStop decoding')
                ffmpeg.stop_decode_media_to_buffer()
                print('Done writing to buffer')

            test_output_video_name = tempfile.mktemp()
            user_input = input('\nTest recording output to file (y/n)? ')
            if user_input == 'y':
                print('\nRecord video to file:')
                ffmpeg.start_recording_media_to_file(test_media_uri, test_output_video_name, 3)

                i = 10
                while i > 0:
                    time.sleep(1)
                    i -= 1
                    if not ffmpeg.is_recorder_running():
                        break

                if i > 0:
                    print('ERROR: ffmpeg stopped because of a timeout (t = %s)' % i)

                print('\nStop recording')
                ffmpeg.stop_recording_media_to_file()
                print('Done recording to file')

            print('\nRunning thread (should be 1)')
            print(threading.enumerate())
            input('\nWhen pressing enter the image file and video file will be deleted...')

            if os.path.isfile(output_image_file):
                os.remove(output_image_file)

            for video_file in get_temp_videos(test_output_video_name):
                os.remove(video_file)

    print("\nDone")
