"""
Binaries package.
"""

import platform
import os


LINUX_FLAVORS = ['Linux']
WINDOWS_FLAVORS = ['Windows']
MACHINE_FLAVORS = ['x86_64', 'x86', 'AMD64']


def get_ffmpeg_bin_path():
    if platform.system() in LINUX_FLAVORS:
        filename = 'ffmpeg'
    elif platform.system() in WINDOWS_FLAVORS:
        filename = 'ffmpeg.exe'
    else:
        raise Exception('Unsupported OS: %s' % platform.system())

    return os.path.join(_get_bin_path(), filename)


def get_ffprobe_bin_path():
    if platform.system() in LINUX_FLAVORS:
        filename = 'ffprobe'
    elif platform.system() in WINDOWS_FLAVORS:
        filename = 'ffprobe.exe'
    else:
        raise Exception('Unsupported OS: %s' % platform.system())

    return os.path.join(_get_bin_path(), filename)


def _get_bin_path():
    current_path = os.path.dirname(__file__)
    os_path = None

    if platform.system() in LINUX_FLAVORS:
        if platform.machine() in MACHINE_FLAVORS:
            os_path = 'Linux64'
    elif platform.system() in WINDOWS_FLAVORS:
        if platform.machine() in MACHINE_FLAVORS:
            os_path = 'Windows64\\bin'

    if os_path is None:
        raise Exception('Unsupported OS: %s %s' % (platform.system(), platform.machine()))

    return os.path.join(current_path, os_path)


if __name__ == "__main__":

    print("FFmpeg bin path :", get_ffmpeg_bin_path())
    print("FFprobe bin path:", get_ffprobe_bin_path())

    print("\nDone")
