# Lily Python FFmpeg

Python wrapper for FFmeg. Including a WX widget for showing decoded video in a WX frame.\
See the examples package for usage.

The FFmpeg binaries are included for Windows and Linux (64 bit only).

## Installation

Installation requirements.

* Python 3
* PIL image library (pip install pillow)
* wxPython (pip install wxpython)

## Features

* Get FFmpeg version
* Get media information from URI/file
* Get image from URI/file
* Output decoded video data from URI/file to a buffer (buffer must have a 'write' method)
* Write decoded video from URI/file to a file (local recording)
* Python WX widget video decoder to use in a WX frame
