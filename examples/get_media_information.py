"""
Get media information.
"""

import json

from ffmpeg import FFmpeg
from test_media import get_default_test_media


ffm = FFmpeg()
media_info = ffm.get_media_info(get_default_test_media())

print(json.dumps(media_info, indent=2))
