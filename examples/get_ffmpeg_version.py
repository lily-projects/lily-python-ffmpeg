"""
Get FFmpeg version.
"""

from ffmpeg import FFmpeg

ffm = FFmpeg()
print(ffm.get_ffmpeg_version())
