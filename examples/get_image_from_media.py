"""
Get image from media.
The image is a PIL image object.
"""

from ffmpeg import FFmpeg
from test_media import get_default_test_media


ffm = FFmpeg()
image = ffm.get_image_from_media(get_default_test_media())
image.show()
