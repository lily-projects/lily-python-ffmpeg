"""
Decode video to a buffer.
Buffer must have a 'write' method.
"""

import time

from ffmpeg import FFmpeg
from test_media import get_default_test_media


class Buffer(object):

    @staticmethod
    def write(frame_data):
        print('Number of bytes:', len(frame_data), 'showing first 40 bytes:')
        print(frame_data[:40])


ffm = FFmpeg()
ffm.start_decode_media_to_buffer(get_default_test_media(), Buffer)
time.sleep(2)
ffm.stop_decode_media_to_buffer()
