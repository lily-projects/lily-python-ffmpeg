"""
Record video to a file.
It will create a .flv (video file) and a log file.
"""

import os
import time

from ffmpeg import FFmpeg
from test_media import get_default_test_media

user_dir = os.path.expanduser('~')
output_name = os.path.join(user_dir, 'recorder_test')

print('The file is here:', user_dir)

ffm = FFmpeg()
ffm.start_recording_media_to_file(get_default_test_media(), output_name)
time.sleep(5)
ffm.stop_recording_media_to_file()
