"""
Shows the decoded video in a WX Python frame.
"""

import wx

from test_media import get_default_test_media
from view_ffmpeg import ViewFFmpeg


class VideoFrame(wx.Frame):

    _WINDOW_TITLE = 'View FFmpeg Test'
    _WINDOW_SIZE = (1000, 600)

    def __init__(self):
        super(VideoFrame, self).__init__(None, -1, self._WINDOW_TITLE, size=self._WINDOW_SIZE)
        self.Bind(wx.EVT_CLOSE, self._on_window_close)

        panel = wx.Panel(self)
        self._view_ffmpeg = ViewFFmpeg(panel)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self._view_ffmpeg, 1, wx.EXPAND)

        panel.SetSizer(box)

        wx.CallAfter(self._start_decoding)

    def _on_window_close(self, event):
        self._view_ffmpeg.stop()
        event.Skip()

    def _start_decoding(self):
        self._view_ffmpeg.set_media_uri(get_default_test_media())
        try:
            self._view_ffmpeg.start()
        except Exception as e:
            dlg = wx.MessageDialog(self, str(e), 'Start Decoder', wx.ICON_EXCLAMATION)
            dlg.ShowModal()
            dlg.Destroy()


if __name__ == '__main__':

    app = wx.App(redirect=False)

    frame = VideoFrame()
    frame.Show()

    app.MainLoop()
